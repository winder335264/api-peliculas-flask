# API-PELICULAS-FLASK

API REST desarrollada en Python con Flask 

## Iniciar el Proyecto

Paso 1: Abrir la consola y ubicarnos en el directorio de la API-PELICULAS-FLASK

Paso 2: Instalar virtualenv: 
```
$> pip install virtualenv
```

Paso 3: Activar el entorno virtual:

Para Linux/Mac:
```
$> source env/bin/activate
```
Para Windows:
```
$> env\Scripts\activate.bat
```


Paso 4: Instalar las librerias que se listan en el archivo requirements.txt
```
$> pip install -r requirements.txt
```
[Para mas Información](https://j2logo.com/virtualenv-pip-librerias-python/)


Paso 5: Conectarse y migrar las tablas a la Base de datos (Flask-Migrate)
```
$> flask db init

$> flask db migrate -m "Initial_db"

$> flask db upgrade
```


Paso 6: Correr la aplicación
```
$> flask run
```

[Tutorial de ApiRest en Flask](https://j2logo.com/flask/tutorial-como-crear-api-rest-python-con-flask/)

